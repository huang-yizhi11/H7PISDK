#include "rz7899.h"

/***********************************************
set rz7899 pwm duty

***********************************************/
void rz_SetPwmDuty(uint32_t channel, uint32_t duty)
{
	 __HAL_TIM_SET_COMPARE(PWM_TIMER, channel, (PWM_TIMER_COUNTER*duty/100));
}

/***********************************************
set rz7899 dir

***********************************************/
void rz_SetDirection(uint32_t dir, uint32_t duty)
{
	if(dir == RZ_DIR_FORWARD)
	{
		rz_SetPwmDuty(PWM_CH1,duty);
		rz_SetPwmDuty(PWM_CH2,0);
	}
	else if(dir == RZ_DIR_BACK)
	{
		rz_SetPwmDuty(PWM_CH1,0);
		rz_SetPwmDuty(PWM_CH2,duty);
	}
}

/***********************************************
rz7899 init

***********************************************/
void rz_Init(void)
{
	rz_SetDirection(RZ_DIR_FORWARD,0);
}
/***********************************************
rz7899 start

***********************************************/
void rz_Start(void)
{
	HAL_TIM_PWM_Start(PWM_TIMER,PWM_CH1);
	HAL_TIM_PWM_Start(PWM_TIMER,PWM_CH2);
}
/***********************************************
rz7899 stop

***********************************************/
void rz_Stop(void)
{
	HAL_TIM_PWM_Stop(PWM_TIMER,PWM_CH1);
	HAL_TIM_PWM_Stop(PWM_TIMER,PWM_CH2);
}