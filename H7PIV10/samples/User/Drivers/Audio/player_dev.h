#ifndef __PLAYER_DEV_H__
#define __PLAYER_DEV_H__

#ifdef __cplusplus
extern "C" {
#endif
#include "stdint.h"


#define PLAYER_BUFFER_SIZE 128
/*****************************************************
 * 
 * player status definition
 * 
 ****************************************************/
typedef enum{
    PLAYER_STOP=0,
    PLAYER_START,
    PLAYER_PAUSE,
    PLAYER_PLAYING,
    PLAYER_INVALID,
    PLAYER_STATUS_NUM
}player_dev_status_t;
/*****************************************************
 * 
 * player dev definition
 * 
 ****************************************************/
typedef struct 
{
    uint32_t  buffer_size;
    void     (*deinit)(void);

    uint8_t  (*stop)(void);
    uint8_t  (*start)(void);

    uint8_t  (*set_samplerate)(uint32_t samplerate);
    uint8_t  (*set_buffer_valid)(uint8_t* buffer, uint8_t state);
    uint8_t  *(*get_buffer_pos)(void);
    player_dev_status_t (*get_status)(void);

    void     (*delay_ms)(uint32_t ms);
    void     (*delay_us)(uint32_t us);
	  void     (*shutdown)(uint8_t sd);
		
}player_dev_t;


#ifdef __cplusplus
}
#endif


#endif

