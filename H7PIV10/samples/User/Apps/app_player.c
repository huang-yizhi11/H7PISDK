#include "app_config.h"

#ifdef app_player

osThreadId app_handler_player;

/*********************************************************************


**********************************************************************/
void app_player_task(void const * argument)
{
	player_dev_t   player_dac;
	//recorder_dev_t recorder_adc;
	//
	//register to system
	sys_device_register(&player_dac,SYS_DEV_PLAYER);
	player_dev_dac_init(&player_dac);
	//sys_device_register(&recorder_adc,SYS_DEV_RECORDER);
	//recorder_dev_adc_init(&recorder_adc);
	//
	//link device driver to wavplayer
	wavplayer_link(&player_dac,NULL); // link player to dac device 

	while(1)
	{	
		wavplayer_shutdown(0);
		wavplayer_player_start("0:/test1.wav");
		wavplayer_shutdown(1);
		osDelay(5000);
	}
}

/*********************************************************************


**********************************************************************/
void app_player_start(void)
{	

  osThreadDef(player_task, app_player_task, osPriorityHigh, 1, 10240);
	app_handler_player = osThreadCreate(osThread(player_task), NULL);
}

#endif